import { WeatherRaw } from './interfaces/weather';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Weather } from './interfaces/City';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { City } from './interfaces/City';


@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  [x: string]: any;
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "bcb45ed52f81cb100b647234c5d06e6d";

  constructor(private http:HttpClient) { }


  private transformWeatherData(data:WeatherRaw):City{
    return {
      name:data.name,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      temperature:data.main.temp,
      humidity:data.main.humidity,
      wind:data.wind.speed
      }
  }

  searchWeatherData(cityName:string):Observable<City>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }




  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
  }
}
