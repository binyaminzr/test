import { getTestBed } from '@angular/core/testing';
import { AuthService } from './../auth.service';
import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from '../interfaces/city';

@Component({
    selector: 'app-students',
    templateUrl: './students.component.html',
    styleUrls: ['./students.component.css']
  })
  export class StudentsComponent implements OnInit {
  cities$:Observable<any>;
  cities:City[] = []
  displayedColumns: string[] = ['name','getdata','temperature','humidity','windspeed','image','predict','result','delete'];
  weatherData$: any;
  hasError: boolean;
  errorMessage: any;
  city: string;
  result$:any;
  owner: any;



  constructor(private PredictionService:PredictionService,private AuthService:AuthService) { }

  predict(index){
    // console.log(index);
    // console.log(this.cities[index]);
    this.result$=this.PredictionService.predict(this.cities[index].temperature,this.cities[index].humidity);
    this.result$=this.result$.subscribe(
      result=>{
        console.log(result);
        if (result>50){
          this.cities[index].predict = "rain";
        }
        else {
          this.cities[index].predict = "no rain";
        }


      }
      
    )
  }

  add(index){
    this.PredictionService.addPredict(this.cities[index].name,this.cities[index].temperature,this.cities[index].humidity,this.cities[index].wind,this.cities[index].predict,this.owner); 
  // this.cities[index].saved = true;
}

deletecity(id){
     console.log(id);
     this.PredictionService.deleteCity(id);
     }



  getDataFun(index, name){
    console.log(name);
    
    this.weatherData$=this.PredictionService.searchWeatherData(name).subscribe(
      data => {
        console.log(data);
        this.cities[index].temperature = data.temperature;
        this.cities[index].image = data.image;
        this.cities[index].humidity = data.humidity;
        this.cities[index].wind = data.wind;
        this.city = name;
     
      },
      error=>{
        console.log(error.message);
        this.hasError= true;
        this.errorMessage=error.message;
      }
    )
  }



  ngOnInit(): void {
    this.AuthService.getUser().subscribe(

      user => {
        this.owner=user.email;
        })
    this.cities$ = this.PredictionService.getCities();
    this.cities$.subscribe(
      docs => {
        this.cities =[];
        for(let document of docs){
          const newCity:City = document.payload.doc.data();
          newCity.id = document.payload.doc.id;
          this.cities.push(newCity);
        }
      }
    )
  }

}