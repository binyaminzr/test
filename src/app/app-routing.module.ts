import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
// import { CityComponent } from './city/city.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { StudentsComponent } from './students/students.component';


const routes: Routes = [ 
{ path: 'login', component: LoginComponent},
{ path: 'signup', component: SignUpComponent},
{ path: 'welcome', component: WelcomeComponent},
// { path: 'city', component: CityComponent},
{ path: 'students', component: StudentsComponent },
{ path: 'studentsForm', component: StudentFormComponent },

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
