export interface City {
    id?:string,
    name:string,
    temperature:number,
    image:string,
    wind:number,
    humidity:number,
    predict?:string,
    saved?:boolean,
    owner?:string,

}
